FROM openjdk:11.0.9.1-jdk
EXPOSE 8080
RUN mkdir /root/app
COPY . /root/app
WORKDIR /root/app
RUN ./mvnw package
CMD java -jar target/microservice.jar